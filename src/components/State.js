import React from "react";
class State extends React.Component{
    constructor(props) {
        super(props);
            this.state = {
                count: this.props.init
        }
    }
    onBtnCountClickUpHandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    onBtnCountClickDownHandler = () => {
        this.setState({
            count: this.state.count - 1
        })
    }
    render() {
        return(
            <div>
                <button onClick = {this.onBtnCountClickUpHandler}>Tăng</button>
                <p>Số Lượt: {this.state.count} </p>
                <button onClick = {this.onBtnCountClickDownHandler}>Giảm</button>
            </div>
        )
    };
}
export default State;